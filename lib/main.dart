import 'package:flutter/material.dart';

void main() {
  runApp(const MyFirstApp());
}

class MyFirstApp extends StatelessWidget {
  const MyFirstApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PSU Trang",
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.insert_emoticon),
          title: Text('My First App'),
          actions: [
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.add_alarm),
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.search),
            ),
          ],
        ),
        body: Center(
          child: Column(
            children: [
              // Image.asset(
              //   'assets/images/cat.jpg',
              //   height: 300,
              //   width: 300,
              // ),

              CircleAvatar(
                backgroundImage: AssetImage('assets/images/cat.jpg',),
                radius: 150,
              ),
              Text(
                'My name is Caty.',
                style: TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// class MyFirstApp extends StatelessWidget {
//   const MyFirstApp({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home:
//     );
//   }
// }
